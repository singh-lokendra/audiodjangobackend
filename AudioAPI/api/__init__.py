from dataclasses import dataclass

from django.http.response import HttpResponse
from rest_framework import status


@dataclass(frozen=True)
class ConstStr:
    audiobook = 'Audiobook'
    podcast = 'Podcast'
    song = 'Song'
    file_type = 'audioFileType'
    meta_data = 'audioFileMetadata'


@dataclass(frozen=True)
class ConstInt:
    str_len = 100
    participant_len = 10


def bad_request():
    return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
