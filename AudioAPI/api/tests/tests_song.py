import json

from django.test import TestCase
from rest_framework import status

from .. import ConstInt
from ..models import Song
from ..serializers import SongSerializer


class SongGetTestCase(TestCase):
    """
    Test the song endpoints.
    """

    def setUp(self):
        self.song1 = Song.objects.create(name='Song 1', duration=10)
        self.song2 = Song.objects.create(name='Song 2', duration=20)
        self.song3 = Song.objects.create(name='Song 3', duration=30)
        self.song4 = Song.objects.create(name='Song 4', duration=40)

    # http://localhost:8000/api/Song/
    def test_get_all(self):
        url = '/api/Song/'
        response = self.client.get(url, format='json')
        song = Song.objects.all()
        serializer = SongSerializer(song, many=True)

        self.assertJSONEqual(str(response.content, encoding='utf8'), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # http://localhost:8000/api/Song/1
    def test_get_single(self):
        url = f'/api/Song/{self.song1.id}/'
        response = self.client.get(url, format='json')
        song = Song.objects.get(id=self.song1.id)
        serializer = SongSerializer(song)

        self.assertJSONEqual(str(response.content, encoding='utf8'), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_id(self):
        url = f'/api/Song/{9999}/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class SongCreateTestCase(TestCase):
    """
    Test the song POST endpoints.

    http://localhost:8000/api/
    """

    def setUp(self):
        self.url = '/api/'
        self.valid_payload = {
            "audioFileType": "Song",
            "audioFileMetadata": {
                "name": "Song 11",
                "duration": 11
            }
        }

    def test_create_valid_song(self):
        response = self.client.post(
            self.url,
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        name = self.valid_payload["audioFileMetadata"]["name"]
        song = Song.objects.get(name=name)
        serializer = SongSerializer(song)

        self.assertJSONEqual(str(response.content, encoding='utf8'), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_file_type(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileType"] = "Songs"
        response = self.client.post(
            self.url,
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_invalid_name_empty(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileMetadata"]["name"] = ""
        response = self.client.post(
            self.url,
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_invalid_name_too_big(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileMetadata"]["name"] = "A" * (ConstInt.str_len + 1)
        response = self.client.post(
            self.url,
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_invalid_duration(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileMetadata"]["duration"] = -1
        response = self.client.post(
            self.url,
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class SongUpdateTestCase(TestCase):
    """
    Test the song POST endpoints.

    http://localhost:8000/api/
    """

    def setUp(self):
        self.song1 = Song.objects.create(name='Song 1', duration=10)
        self.url = f'/api/Song/{self.song1.id}/'
        self.valid_payload = {
            "audioFileType": "Song",
            "audioFileMetadata": {
                "name": "Song 21",
                "duration": 21
            }
        }

    def test_update_valid_song(self):
        response = self.client.put(
            self.url,
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        song = Song.objects.get(id=self.song1.id)
        serializer = SongSerializer(song)

        self.assertJSONEqual(str(response.content, encoding='utf8'), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_invalid_file_type(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileType"] = "Songs"
        response = self.client.put(
            f'/api/{invalid_payload["audioFileType"]}/{self.song1.id}/',
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_invalid_name_empty(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileMetadata"]["name"] = ""
        response = self.client.put(
            self.url,
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_invalid_name_too_big(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileMetadata"]["name"] = "A" * (ConstInt.str_len + 1)
        response = self.client.put(
            self.url.format(self.song1.id),
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_invalid_duration(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileMetadata"]["duration"] = -1
        response = self.client.put(
            self.url.format(self.song1.id),
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class SongDeleteTestCase(TestCase):
    """
    Test the song endpoints.
    """

    def setUp(self):
        self.song1 = Song.objects.create(name='Song 1', duration=10)
        self.song2 = Song.objects.create(name='Song 2', duration=20)
        self.song3 = Song.objects.create(name='Song 3', duration=30)
        self.song4 = Song.objects.create(name='Song 4', duration=40)

    # http://localhost:8000/api/Song/1
    def test_delete_single(self):
        url = f'/api/Song/{self.song1.id}/'
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_id(self):
        url = f'/api/Song/{9999}/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
