import json

from django.test import TestCase
from rest_framework import status

from .. import ConstInt
from ..models import Podcast
from ..serializers import PodcastSerializer


class PodcastGetTestCase(TestCase):
    """
    Test the podcast endpoints.
    """

    def setUp(self):
        data1 = {
            'name': 'Podcast 1',
            'host': 'host 1',
            'duration': 10,
            'participants': [
                {"name": "p2"},
                {"name": "p1"},
                {"name": "p3"},
            ]
        }

        data2 = {
            'name': 'Podcast 2',
            'host': 'host 2',
            'duration': 20,
            'participants': [
                {"name": "p11"},
                {"name": "p12"},
                {"name": "p13"},
            ]
        }

        podcast1 = PodcastSerializer(data=data1)
        podcast2 = PodcastSerializer(data=data2)

        if podcast1.is_valid(True):
            self.podcast1 = podcast1.save()
        if podcast2.is_valid(True):
            self.podcast2 = podcast2.save()

    # http://localhost:8000/api/Podcast/
    def test_get_all(self):
        url = '/api/Podcast/'
        response = self.client.get(url, format='json')
        podcast = Podcast.objects.all()
        serializer = PodcastSerializer(podcast, many=True)

        self.assertJSONEqual(str(response.content, encoding='utf8'), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # http://localhost:8000/api/Podcast/1
    def test_get_single(self):
        print(self.podcast1)
        url = f'/api/Podcast/{self.podcast1.id}/'
        response = self.client.get(url, format='json')
        podcast = Podcast.objects.get(id=self.podcast1.id)
        serializer = PodcastSerializer(podcast)

        self.assertJSONEqual(str(response.content, encoding='utf8'), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_id(self):
        url = f'/api/Podcast/{9999}/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class PodcastCreateTestCase(TestCase):
    """
    Test the podcast POST endpoints.

    http://localhost:8000/api/
    """

    def setUp(self):
        self.url = '/api/'
        self.valid_payload = {
            "audioFileType": "Podcast",
            "audioFileMetadata": {
                'name': 'Podcast 1',
                'host': 'host 1',
                'duration': 10,
                'participants': [
                    {"name": "p2"},
                    {"name": "p1"},
                    {"name": "p3"},
                ]
            }
        }

    def test_create_valid_podcast(self):
        response = self.client.post(
            self.url,
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        name = self.valid_payload["audioFileMetadata"]["name"]
        podcast = Podcast.objects.get(name=name)
        serializer = PodcastSerializer(podcast)

        self.assertJSONEqual(str(response.content, encoding='utf8'), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_file_type(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileType"] = "Podcasts"
        response = self.client.post(
            self.url,
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_invalid_name_empty(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileMetadata"]["name"] = ""
        response = self.client.post(
            self.url,
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_invalid_name_too_big(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileMetadata"]["name"] = "A" * (ConstInt.str_len + 1)
        response = self.client.post(
            self.url,
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_invalid_duration(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileMetadata"]["duration"] = -1
        response = self.client.post(
            self.url,
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_invalid_participants(self):
        invalid_payload = self.valid_payload
        participants = [{"name": "p2"}] * (ConstInt.participant_len + 1)

        invalid_payload["audioFileMetadata"]["participants"] = participants
        response = self.client.post(
            self.url,
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class PodcastUpdateTestCase(TestCase):
    """
    Test the podcast POST endpoints.

    http://localhost:8000/api/
    """

    def setUp(self):
        self.podcast1 = Podcast.objects.create(name='Podcast 1', duration=10)
        self.url = f'/api/Podcast/{self.podcast1.id}/'
        self.valid_payload = {
            "audioFileType": "Podcast",
            "audioFileMetadata": {
                'name': 'Podcast 1',
                'host': 'host 1',
                'duration': 10,
                'participants': [
                    {"name": "p2"},
                    {"name": "p1"},
                    {"name": "p3"},
                ]
            }
        }

    def test_update_valid_podcast(self):
        response = self.client.put(
            self.url,
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        podcast = Podcast.objects.get(id=self.podcast1.id)
        serializer = PodcastSerializer(podcast)

        self.assertJSONEqual(str(response.content, encoding='utf8'), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_invalid_file_type(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileType"] = "Podcasts"
        response = self.client.put(
            f'/api/{invalid_payload["audioFileType"]}/{self.podcast1.id}/',
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_invalid_name_empty(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileMetadata"]["name"] = ""
        response = self.client.put(
            self.url,
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_invalid_name_too_big(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileMetadata"]["name"] = "A" * (ConstInt.str_len + 1)
        response = self.client.put(
            self.url.format(self.podcast1.id),
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_invalid_duration(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileMetadata"]["duration"] = -1
        response = self.client.put(
            self.url.format(self.podcast1.id),
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_invalid_participants(self):
        invalid_payload = self.valid_payload
        participants = [{"name": "p2"}] * (ConstInt.participant_len + 1)

        invalid_payload["audioFileMetadata"]["participants"] = participants
        response = self.client.put(
            self.url.format(self.podcast1.id),
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class PodcastDeleteTestCase(TestCase):
    """
    Test the podcast endpoints.
    """

    def setUp(self):
        data1 = {
            'name': 'Podcast 1',
            'host': 'host 1',
            'duration': 10,
            'participants': [
                {"name": "p2"},
                {"name": "p1"},
                {"name": "p3"},
            ]
        }

        data2 = {
            'name': 'Podcast 2',
            'host': 'host 2',
            'duration': 20,
            'participants': [
                {"name": "p11"},
                {"name": "p12"},
                {"name": "p13"},
            ]
        }

        podcast1 = PodcastSerializer(data=data1)
        podcast2 = PodcastSerializer(data=data2)

        if podcast1.is_valid(True):
            self.podcast1 = podcast1.save()

    # http://localhost:8000/api/Podcast/1
    def test_delete_single(self):
        url = f'/api/Podcast/{self.podcast1.id}/'
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_id(self):
        url = f'/api/Podcast/{9999}/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
